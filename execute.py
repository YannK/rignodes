# SPDX-License-Identifier: GPL-2.0-or-later

is_first_load = "nodes" not in locals()
from . import nodes

if not is_first_load:
    import sys

    nodes = sys.modules[nodes.__name__]

import bpy

_skip_next_autorun = False


modes_enum = [
    ("AUTO", "Automatic", ""),
    ("FORWARD", "Forward", ""),
    ("BACKWARD", "Backward", ""),
]


class RigNodes_OT_execute_tree(bpy.types.Operator):
    bl_idname = "rignodes.execute_tree"
    bl_label = "Execute"
    bl_options = {"REGISTER", "UNDO"}

    def execute(self, context: bpy.types.Context) -> set[str]:
        rignodes_mode = context.scene.rignodes_mode
        if rignodes_mode == "AUTO":
            rignodes_mode = _choose_auto_mode(context)

        execute_tree(
            context.space_data.edit_tree,
            context.view_layer.depsgraph,
            rignodes_mode,
        )

        # Redraw the 'Has Run' properties.
        context.area.tag_redraw()

        return {"FINISHED"}


def execute_tree(
    tree: nodes.RigNodesNodeTree,
    depsgraph: bpy.types.Depsgraph,
    mode: str,
) -> None:
    # The running of this tree will trigger another depsgraph update, which
    # should not trigger yet another execution.
    global _skip_next_autorun

    try:
        tree.run_event(depsgraph, mode)
    except Exception:
        # Disable auto-run on exceptions.
        print(f"Disabling auto-run on {tree} due to an exception")
        tree.autorun = False
        raise
    finally:
        _skip_next_autorun = True


@bpy.app.handlers.persistent  # type: ignore
def _on_depsgraph_update_post(
    scene: bpy.types.Scene, depsgraph: bpy.types.Depsgraph
) -> None:
    global _skip_next_autorun

    if _skip_next_autorun:
        _skip_next_autorun = False
        return

    run_node_tree(scene, depsgraph)


@bpy.app.handlers.persistent  # type: ignore
def _on_frame_changed_post(
    scene: bpy.types.Scene, depsgraph: bpy.types.Depsgraph
) -> None:
    global _skip_next_autorun
    _skip_next_autorun = True

    run_node_tree(scene, depsgraph)


def run_node_tree(scene: bpy.types.Scene, depsgraph: bpy.types.Depsgraph) -> None:
    for tree in bpy.data.node_groups:
        if tree.bl_idname != "RigNodesNodeTree":
            continue
        if tree.is_running:
            # This tree is still running, AND triggered a depsgraph update.
            # Since it's still running, there will likely be another one that'll
            # need skipping.
            _skip_next_autorun = True
            continue
        if not tree.autorun:
            return

        rignodes_mode = scene.rignodes_mode
        if rignodes_mode == "AUTO":
            # TODO: don't use the global context here.
            rignodes_mode = _choose_auto_mode(bpy.context)

        execute_tree(tree, depsgraph, rignodes_mode)


def _choose_auto_mode(context: bpy.types.Context) -> str:
    if context.object and context.object.mode == "POSE":
        return "BACKWARD"
    return "FORWARD"


classes = (
    # Operators:
    RigNodes_OT_execute_tree,
)
_register, _unregister = bpy.utils.register_classes_factory(classes)


def register() -> None:
    _register()

    bpy.types.Scene.rignodes_mode = bpy.props.EnumProperty(
        name="Mode",
        items=modes_enum,
    )

    bpy.app.handlers.depsgraph_update_post.append(_on_depsgraph_update_post)
    bpy.app.handlers.frame_change_post.append(_on_frame_changed_post)


def unregister() -> None:
    bpy.app.handlers.depsgraph_update_post.remove(_on_depsgraph_update_post)
    bpy.app.handlers.frame_change_post.remove(_on_frame_changed_post)

    del bpy.types.Scene.rignodes_mode

    _unregister()
